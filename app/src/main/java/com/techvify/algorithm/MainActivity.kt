package com.techvify.algorithm

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSubmitCandies.setOnClickListener {
            var inputList = parseInputList(edtInputCandies.text.toString())
            tvResultCandies.text = countMinimumCandies(inputList).toString()
        }

        btnSubmitLeftRotation.setOnClickListener {
            tvResultLeftRotation.text = leftRotation(parseInputList(edtInputCandies.text.toString())).toString()
        }
    }

    private fun parseInputList(input: String): IntArray {
        return input.split(" ").map { Integer.parseInt(it) }.toIntArray()
    }


    fun countMinimumCandies(arr: IntArray): Int {
        var n = arr.size
        val res = IntArray(n)
        for (i in 0 until n) res[i] = 1

        //forward
        for (i in 1 until n) {
            if (arr[i - 1] < arr[i] && res[i - 1] >= res[i]) {
                res[i] = res[i - 1] + 1
            }
        }

        //backward
        for (i in n - 1 downTo 1) {
            if (arr[i - 1] > arr[i] && res[i - 1] <= res[i]) {
                res[i - 1] = res[i] + 1
            }
        }
        var sum = 0
        for (j in res) sum += j
        return sum
    }

    private fun rotateArray(A: IntArray) {
        val t = A[0]
        for (i in 0 until A.size - 1) {
            A[i] = A[i + 1]
        }
        A[A.size - 1] = t
    }

    fun leftRotation(arr: IntArray) {
        val d = arr.size
        for (i in 0 until d) {
            rotateArray(arr)
        }
        for (a in arr) {
            print("$a ")
        }
    }
}